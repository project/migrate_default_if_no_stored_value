<?php

namespace Drupal\migrate_default_if_no_stored_value\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\MigrateSkipProcessException;

/**
 * Sets a default value only if the destination has no stored value already.
 *
 * This is useful if you are migrating content in, want to set a default
 * value, but may adjust the value itself in Drupal afterwards and do not
 * wish to destroy the value every single time.
 *
 * Available configuration keys:
 * - default_value: The fixed default value to apply.
 * - entity_type: The fixed default value to apply.
 *
 * @MigrateProcessPlugin(
 *   id = "default_if_no_stored_value",
 *   handle_multiples = TRUE
 * )
 */
class DefaultIfNoStoredValue extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Check that all required config is in place.
    if (!isset($this->configuration['default_value'])) {
      throw new MigrateSkipProcessException('Not all required config values were found, skipping processing for destination: ' . $destination_property);
    }

    $source_dest_mapping = $row->getIdMap();

    // Uses the default value if the item has not been imported yet.
    if (!isset($source_dest_mapping['destid1'])) {
      return $this->configuration['default_value'];
    }

    // If the destination property has a subfield, then this determines the
    // both the field and subfield names. When it looks up existing values, then
    // this will find the values of that subfield.
    // E.g., If the destination property is
    // targeting "field_body/value", then this will determine the field name,
    // which is "field_body", along with the subfield (e.g., "value").
    if (str_contains($destination_property, $row::PROPERTY_SEPARATOR)) {
      $destination_field_parts = explode($row::PROPERTY_SEPARATOR, $destination_property);
      $destination_field_name = $destination_field_parts[0];
      $destination_field_sub_property = $destination_field_parts[1];
    }
    else {
      $destination_field_name = $destination_property;
    }

    $destination_id = $source_dest_mapping['destid1'];
    $entity_type = $this->configuration['entity_type'] ?? 'node';

    if (intval($destination_id) > 0) {
      // Try to load the entity based on the destination id.
      $entity_storage = $this->entityTypeManager->getStorage($entity_type);
      $entity = $entity_storage->load($destination_id);

      // Make sure we have a result, and just one result.
      if ($entity) {
        // Load the entity & if it has a value, is it, otherwise just proceed.
        if (
          $entity->get($destination_field_name)
          && $entity->get($destination_field_name)->first()
          && !$entity->get($destination_field_name)->first()->isEmpty
        ) {
          if (isset($destination_field_sub_property)) {
            $field_value = $entity->{$destination_field_name}->first()->getValue()[$destination_field_sub_property];
          }
          else {
            $field_value = $entity->{$destination_field_name}->first()->getString();
          }

          return $field_value;
        }
      }
    }

    // If a case didn't return a value, use default value after all.
    return $this->configuration['default_value'];
  }

}
